package handlers

import (
	"net/http"

	"time"

	"gitlab.com/verminio/gofw/web"
)

func init() {
	h := []web.Route{
		web.Route{
			"Hello",
			http.MethodGet,
			"/hello",
			"Hello World",
			Hello,
			[]web.Middleware{},
		},
	}

	web.Register(h)
}

func Hello(r *web.RequestWrapper, ctx web.AppContext) {
	t := ctx.TraceManager.Info("In Handler", nil)
	time.Sleep(5 * time.Millisecond)
	r.Entity = "Hello World!"
	r.TraceList.Append(t)
}

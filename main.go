package main

import (
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"

	"gitlab.com/verminio/gofw/tracing/std"
	"gitlab.com/verminio/gofw/web"

	_ "gitlab.com/verminio/gofw-sample/handlers"
)

var r *mux.Router

var host string
var port int

var logger *log.Logger

func init() {
	logger = log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)

	m := std.New(logger)

	ctx := web.AppContext{
		TraceManager: m,
	}
	logger.Println("Initializing router")
	r = web.NewRouter(ctx)

	host = "localhost"
	port = 9090
	logger.Println("Completed router initialization")
}

func main() {
	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(host+":"+strconv.Itoa(port), r))
}
